import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public storage: Storage) {
    
  }
setfunc(){
this.storage.set('myval','inside setting data');
}
getfunc(){
this.storage.get('myval').then((val) => {
       console.log('data - ', val);
     })
}
testingfunc(){
  console.log("testing git integration");
}
}
